# RESELOGIC

RESELOGIC est une plateforme de gestion des ventes directes. Elle permet la visualisation de l'activité commerciales en temps réels.

# Modules

- Authentification
    - Gestion des utilisateurs
    - Gestion des rôles
    - Gestion des permissions
    - Log de l'activité des utilisateurs sur la plateforme

- Client
    - Gestion des clients
    - Gestion des comptes clients
    - Gestion des emplacements géographiques des clients

- RH
    - Gestion des employées
    - Gestion des comptes employées

- Produits
    - Gestion des catégories
    - Gestion des produits

- Parc automobile
    - Gestion des véhicules
    - Attribution des véhicules au employées

- Commandes
    - Gestion des commandes clients

- Vente directe
    - Gestion des zones géographiques de distribution/prospection
    - Choix de la zone géographique de distribution/prospection
    - Suggestion de l'itinéraire de distribution/prospection
    - Suivie en temps réel de la distribution/prospection

- Stock
    - Gestion des points de distribution
    - Gestion des entrées / sorties du stock
