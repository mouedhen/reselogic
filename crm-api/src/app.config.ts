export const AppConfig = {
    globalPath: 'reselogic/api/services',
    portToListen: 3000,
    saltRounds: 10,
    jwtSecret: 'p1ou_q69H_I7cYVqd8FecsRxLMC78avXrHVHERfU5VQ',
    jwtLifeTime:'1d'
}