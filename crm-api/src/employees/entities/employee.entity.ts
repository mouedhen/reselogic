import { BaseEntity } from "src/communs/entities/base-entity";
import { Entity, Column, JoinColumn, OneToOne, ManyToOne } from "typeorm";
import { Users } from "src/users/entities/users.entity";
import { Vehicules } from "src/vehicules/entities/vehicules.entity";

@Entity()
export class Employees extends BaseEntity{
    @Column()
    firstName: string;
    @Column()
    lastName: string;
    @Column({unique: true})
    registrationNumber: string;
    @Column()
    address: string;
    @Column({ unique: true, nullable: true })
    email: string;
    @Column()
    phoneNumber: number;
    @Column({ nullable: true})
    function?: string;
    @Column()
    startDate: Date;
    @Column({ nullable: true })
    endDate: Date;
    @Column()
    birthDate:Date;
    @OneToOne(type => Users, user => user.employee)
    @JoinColumn()
    user: Users;
    @ManyToOne(type => Vehicules, vehicule => vehicule.employee)
    vehicule: Vehicules;
}