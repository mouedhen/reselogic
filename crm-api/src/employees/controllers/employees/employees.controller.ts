import { Controller, UseGuards, Get, Post, Req, Body, Put } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/services/jwt-auth.guard';
import { Employees } from 'src/employees/entities/employee.entity';
import { BaseParams } from 'src/communs/entities/base-params';
import { Request } from 'express';
import { Users } from 'src/users/entities/users.entity';
import { EmployeesService } from 'src/employees/services/employees/employees.service';

@UseGuards(JwtAuthGuard)
@Controller('employees')
export class EmployeesController {

    employeesParams: BaseParams = new BaseParams();
    constructor(private readonly employeeService: EmployeesService){
        this.employeesParams.currentRepository = "Employees"
        this.employeesParams.relations = [ "vehicule" ]
    }

    @Get()
    async getAllEmployees(): Promise<Employees[]>{
        const employeesList = await this.employeeService.findAll(this.employeesParams,true);
        return employeesList;
    }

    @Post()
    async addEmployee(@Req() req: Request, @Body() employee: Employees ):Promise<Employees>{
        const currentUser:Users = <Users>(req.user);
        const newEmployee = await this.employeeService.add(employee, this.employeesParams,currentUser);
        return newEmployee;
    }

    @Put()
    async updateEmployee(@Req() req: Request, @Body() employee: Employees ):Promise<Employees>{
        const currentUser:Users = <Users>(req.user);
        const updatedEmployee = await this.employeeService.update(employee,employee.pk, this.employeesParams,currentUser);
        return updatedEmployee;
    }

}
