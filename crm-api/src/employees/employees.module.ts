import { Module } from '@nestjs/common';
import { EmployeesController } from './controllers/employees/employees.controller';
import { AuthModule } from 'src/auth/auth.module';
import { EmployeesService } from './services/employees/employees.service';

@Module({
    imports: [ AuthModule ],
    controllers: [ EmployeesController ],
    providers: [ EmployeesService ]
})
export class EmployeesModule {}
