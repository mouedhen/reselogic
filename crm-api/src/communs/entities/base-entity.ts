import { IBaseEntity } from "../interfaces/ibase-entity.interface";
import { EntityStatus } from "./entity-status.enum";
import { PrimaryGeneratedColumn, Column } from "typeorm";

export class BaseEntity implements IBaseEntity{
    @PrimaryGeneratedColumn()
    pk: number;
    @Column()
    created: Date;
    @Column({nullable: true})
    updated?: Date;
    @Column({nullable: true})
    deleted?: Date;
    @Column()
    createdU: number;
    @Column({nullable: true})
    updatedU?: number;
    @Column({nullable: true})
    deletedU?: number;
    @Column()
    entityStatus: EntityStatus;
}