import { BaseParams } from "../entities/base-params";
import { Users } from "src/users/entities/users.entity";

export interface IBaseService{
    add(entity: any, baseParams: BaseParams, currentUser: Users):Promise<any>;
    update(entity: any,id: number, baseParams: BaseParams, currentUser: Users): Promise<any>;
    delete(id:number, baseParams: BaseParams, currentUser: Users): Promise<any>;
    findOne(params: any, baseParams: BaseParams):Promise<any>;
    findByParams(params: any[], baseParams: BaseParams):Promise<any[]>;
    findAll(baseParams: BaseParams):Promise<any[]>;
}