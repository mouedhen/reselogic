import { EntityStatus } from "../entities/entity-status.enum";

export interface IBaseEntity{
    pk: number;
    created: Date;
    updated?: Date;
    deleted?: Date;
    createdU: number;
    updatedU?: number;
    deletedU?: number;
    entityStatus: EntityStatus;
}