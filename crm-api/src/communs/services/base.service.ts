import { IBaseService } from "../interfaces/ibase-service.interface";
import { EntityStatus } from "../entities/entity-status.enum";
import { getRepository } from "typeorm";
import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { BaseParams } from "../entities/base-params";
import { Users } from "src/users/entities/users.entity";

@Injectable()
export class BaseService implements IBaseService{
    repository;
    
    async add(entity: any,baseParams: BaseParams, currentUser: Users): Promise<any> {
        try{
            this.repository = getRepository(baseParams.currentRepository);
            const newEntity = <any>(this.repository.create(entity));
            newEntity.created = new Date();
            newEntity.entityStatus = EntityStatus.CREATED;
            newEntity.createdU = currentUser.pk;
            await this.repository.save(newEntity);
            return newEntity;
        }catch(exception){
            throw new HttpException(exception.toString(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async update(entity: any, id: number, baseParams: BaseParams, currentUser: Users): Promise<any> {
        try{
            this.repository = getRepository(baseParams.currentRepository);
            const updatedEntity = <any>(entity);
            updatedEntity.updated = new Date();
            updatedEntity.entityStatus = EntityStatus.UPDATED;
            updatedEntity.updatedU = currentUser.pk;
            await this.repository.update(id, updatedEntity);
            return this.repository.findOne(id);
        }catch(exception){
            throw new HttpException(exception.toString(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    delete(id: number, baseParams: BaseParams): Promise<any> {
        throw new Error("Method not implemented.");
    }
    

    async findOne(params: any, baseParams: BaseParams, includeRelations = false): Promise<any> {
        try{
            this.repository = getRepository(baseParams.currentRepository);
            if(includeRelations)    
                return await this.repository.findOne(
                { 
                    where: params,
                    relations:baseParams.relations 
                });
            return await this.repository.findOne({ where: params });
        }catch(exception){
            throw new HttpException(exception.toString(),HttpStatus.INTERNAL_SERVER_ERROR);
        }   
    }
    
    findByParams(params: any[], baseParams: BaseParams): Promise<any[]> {
        throw new Error("Method not implemented.");
    }
    async findAll(baseParams: BaseParams, includeRelations = false): Promise<any[]> {
        try{
            this.repository = getRepository(baseParams.currentRepository);
            
            if(includeRelations)
                return await this.repository.find({ relations: baseParams.relations });

            return await this.repository.find();
        }catch(exception){
            throw new HttpException(exception.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}