import { DataBases } from "./data-bases.config";

export const OrmConfig = {
    type: DataBases[0].type,
    host: DataBases[0].host,
    port: DataBases[0].port,
    username: DataBases[0].username,
    password: DataBases[0].password,
    database: DataBases[0].database,
    entities: DataBases[0].entities,
    synchronize: DataBases[0].synchronize,
}
