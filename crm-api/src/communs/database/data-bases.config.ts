export const DataBases = [
    {
        type: "mysql",
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: "",
        database: 'reselogic',
        entities: ["dist/**/*.entity{.ts,.js}"],
        synchronize: true,
    }
]