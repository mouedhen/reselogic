import { Module } from '@nestjs/common';
import { UsersController } from './controllers/users.controller';
import { BaseService } from 'src/communs/services/base.service';
import { UserService } from './services/users.service';
import { PermissionsController } from './controllers/permissions.controller';
import { AuthModule } from 'src/auth/auth.module';
import { RolesController } from './controllers/roles.controller';

@Module({
    imports:[ AuthModule ],
    controllers: [ UsersController, PermissionsController, RolesController ],
    providers:[
        BaseService,
        UserService
    ]
})
export class UsersModule {}
