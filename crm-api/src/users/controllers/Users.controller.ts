import { Controller, Post, Body, Put, UseGuards, Get, Req } from "@nestjs/common";
import { Users } from "../entities/users.entity";
import { BaseParams } from "src/communs/entities/base-params";
import { UserService } from "../services/users.service";
import { JwtAuthGuard } from "src/auth/services/jwt-auth.guard";
import { BaseService } from "src/communs/services/base.service";
import { Request } from "express";

@UseGuards(JwtAuthGuard)
@Controller('users')
export class UsersController {
    userBaseParams: BaseParams = new BaseParams();
    constructor(private readonly userService: UserService, private readonly baseService: BaseService) {
        this.userBaseParams.currentRepository = 'Users'; 
    }

    @Post()
    async addUser(@Req() req: Request, @Body() user: Users):Promise<Users>{
        const currentUser:Users = <Users>(req.user);
        const newUser: Users = await this.userService.addUser(user,this.userBaseParams,currentUser);
        newUser.password = undefined;
        return newUser;
    }

    @Put()
    async updateUser(@Req() req: Request,@Body() user: Users): Promise<Users>{
        const currentUser:Users = <Users>(req.user);
        const updateUser: Users = await this.userService.updateUser(user,this.userBaseParams,currentUser);
        updateUser.password = undefined;
        return updateUser;
    }

    @Get()
    async listUsers():Promise<Users[]>{
        const usersList: Users[] = await this.baseService.findAll(this.userBaseParams);
        return usersList;
    }
}