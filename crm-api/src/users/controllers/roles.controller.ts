import { Controller, Post, Body, Get, Put, UseGuards, Req } from "@nestjs/common";
import { BaseParams } from "src/communs/entities/base-params";
import { BaseService } from "src/communs/services/base.service";
import { JwtAuthGuard } from "src/auth/services/jwt-auth.guard";
import { Roles } from "../entities/Roles.entity";
import { Request } from "express";
import { Users } from "../entities/users.entity";

@UseGuards(JwtAuthGuard)
@Controller('roles')
export class RolesController{
    roleBaseParams: BaseParams = new BaseParams();
    constructor(private readonly baseService: BaseService) {
        this.roleBaseParams.currentRepository = 'Roles';
    }

    @Post()
    async addRole(@Req() req:Request ,@Body() role: Roles):Promise<Roles>{
        const currentUser:Users = <Users>(req.user);
        const newrole = await this.baseService.add(role, this.roleBaseParams,currentUser);
        return newrole;
    }

    @Put()
    async updateRole(@Req() req:Request,@Body() role: Roles):Promise<Roles>{
        const currentUser:Users = <Users>(req.user);
        const updatedrole = await this.baseService.update(role,role.pk,this.roleBaseParams,currentUser);
        return updatedrole;
    }

    @Get()
    async getAllRoles(): Promise<Roles[]>{
        const roles = await this.baseService.findAll(this.roleBaseParams);
        return roles;
    }

}