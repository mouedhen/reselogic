import { Controller, Post, Body, Get, Put, UseGuards, Req } from "@nestjs/common";
import { BaseParams } from "src/communs/entities/base-params";
import { BaseService } from "src/communs/services/base.service";
import { Permissions } from "../entities/permissions.entity";
import { JwtAuthGuard } from "src/auth/services/jwt-auth.guard";
import { Request } from "express";
import { Users } from "../entities/users.entity";

@UseGuards(JwtAuthGuard)
@Controller('permissions')
export class PermissionsController{
    permissionBaseParams: BaseParams = new BaseParams();
    constructor(private readonly baseService: BaseService) {
        this.permissionBaseParams.currentRepository = 'Permissions'; 
    }

    @Post()
    async addPermission(@Req() req: Request, @Body() permission: Permissions):Promise<Permissions>{
        const user: Users = <Users>req.user;
        const newPermission = await this.baseService.add(permission, this.permissionBaseParams,user);
        return newPermission;
    }

    @Put()
    async updatePermission(@Req() req: Request, @Body() permission: Permissions):Promise<Permissions>{
        const user: Users = <Users>req.user;
        const updatedPermission = await this.baseService.update(permission,permission.pk,this.permissionBaseParams,user);
        return updatedPermission;
    }

    @Get()
    async getAllPermissions(): Promise<Permissions[]>{
        const permissions = await this.baseService.findAll(this.permissionBaseParams);
        return permissions;
    }

}