import { Users } from "../entities/users.entity";
import { BaseParams } from "src/communs/entities/base-params";

export interface IUsersService{
    addUser(user: Users,baseParams: BaseParams,currentUser: Users): Promise<Users>;
    updateUser(user: Users, baseParams: BaseParams,currentUser: Users): Promise<Users>;
}