import { IUsersService } from "../interfaces/iuser-service.interface";
import { Users } from "../entities/users.entity";
import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { BaseService } from "src/communs/services/base.service";
import { BaseParams } from "src/communs/entities/base-params";
import * as bcrypt from 'bcrypt';
import { AppConfig } from "src/app.config";

@Injectable()
export class UserService extends BaseService implements IUsersService{
    constructor(){
        super();
    }
    
    async addUser(user: Users, baseParams: BaseParams, currentConnectedUser:Users): Promise<Users> {
        try{
            const cryptedPass = await bcrypt.hash(user.password,AppConfig.saltRounds);
            const currentUser = await this.findOne({login : user.login},baseParams);
            if(currentUser){
                throw new HttpException("User already exists", HttpStatus.BAD_REQUEST); 
            }
            if(cryptedPass) {
                user.password = cryptedPass;
                return this.add(user,baseParams,currentConnectedUser); 
            }   
        }catch(error){
            throw new HttpException(error.toString(),HttpStatus.BAD_REQUEST);
        }
    }

    async updateUser(user: Users, baseParams: BaseParams, currentUser:Users): Promise<Users>{
        try{
            return this.update(user, user.pk, baseParams,currentUser);
        }catch(error){
            throw new HttpException(error.toString(), HttpStatus.BAD_REQUEST)
        }
    }
    
}