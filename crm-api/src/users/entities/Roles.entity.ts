import { Permissions } from "./permissions.entity";
import { Entity, Column, ManyToMany, JoinTable } from "typeorm";
import { BaseEntity } from "src/communs/entities/base-entity";

@Entity()
export class Roles extends BaseEntity{
    @Column({unique: true})
    identifier: string;
    @Column({ type:'text', nullable: true})
    description: string;
    @Column({ default: true })
    isAttribuatable: boolean;
    @ManyToMany( () => Permissions, { cascade:true })
    @JoinTable()
    permissions: Permissions[];
}