import { Permissions } from "./permissions.entity";
import { Entity, Column, ManyToMany, JoinTable, OneToOne } from 'typeorm';
import { BaseEntity } from "src/communs/entities/base-entity";
import { Roles } from "./Roles.entity";
import { Employees } from "src/employees/entities/employee.entity";

@Entity()
export class Users extends BaseEntity{
    
    @Column({unique: true})
    login: string;
    
    @Column()
    password: string;
    
    @ManyToMany( () => Permissions, { cascade:true })
    @JoinTable()
    permissions: Permissions[];

    @ManyToMany( () => Roles, { cascade:true })
    @JoinTable()
    roles: Roles[];
    
    @OneToOne(type => Employees, employee => employee.user)
    employee: Employees;
}