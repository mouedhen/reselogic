import { Entity, Column } from "typeorm";
import { BaseEntity } from "src/communs/entities/base-entity";

@Entity()
export class Permissions extends BaseEntity{
    @Column({unique: true})
    identifier: string;
    @Column()
    endPoint: string;
    @Column()
    method: string;
    @Column({ type:'text', nullable: true})
    description: string;
    @Column({default: true})
    isActive: boolean;
}