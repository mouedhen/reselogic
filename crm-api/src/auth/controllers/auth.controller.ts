import { Controller, Post, UseGuards, HttpCode, Request, Res } from "@nestjs/common";
import { AuthService } from "../services/auth.service";
import { BaseParams } from "src/communs/entities/base-params";
import { LocalAuthenticationGuard } from "../services/localAuthentication.guard";
import { Response } from "express";

@Controller('authentication')
export class AuthController{
    
    authParams:BaseParams = new BaseParams();
    
    constructor(private readonly authService:AuthService){
        this.authParams.currentRepository = 'Users';
    }

    @HttpCode(200)
    @UseGuards(LocalAuthenticationGuard)
    @Post()
    async logIn(@Request() request, @Res() response: Response) {
        const user = request.user;
        const jwtToken = await this.authService.generateAccessToken(user);
        response.setHeader('token',jwtToken)
        user.password = undefined;
        return response.send(user);
    }
}