import { AuthGuard } from "@nestjs/passport";
import { ExecutionContext, Injectable, HttpException, HttpStatus } from "@nestjs/common";

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt'){
    
    canActivate(context: ExecutionContext){
        return super.canActivate(context);
    }

    handleRequest(err, user, info) {
        if (err || !user) {
            throw err || new HttpException(info.message,HttpStatus.UNAUTHORIZED);
        }
        return user;
    }

} 