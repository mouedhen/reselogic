import { PassportStrategy } from "@nestjs/passport";
import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { AppConfig } from "src/app.config";
import { JwtPayload } from "../interfaces/jwt-payload.interface";
import { Users } from "src/users/entities/users.entity";
import { AuthService } from "./auth.service";
import { ExtractJwt, Strategy  } from "passport-jwt";
import { Request } from "express";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authService:AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: AppConfig.jwtSecret,
            passReqToCallback: true
        });
    }
    async validate(req:Request,payload: JwtPayload): Promise<Users> {
        const user = await this.authService.checkUser(payload.username,payload.password,req);
        if (!user) {
            throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);    
        }    
        return user;  
    }
}