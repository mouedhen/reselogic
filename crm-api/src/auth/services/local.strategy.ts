import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { Users } from 'src/users/entities/users.entity';
import { AuthService } from './auth.service';
import { ModuleRef } from '@nestjs/core';
 
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private authService: AuthService,private moduleRef: ModuleRef) {
        super();
    }
  async validate(username: string, password: string): Promise<Users> {
    return this.authService.authenticate(username, password);
  }
}