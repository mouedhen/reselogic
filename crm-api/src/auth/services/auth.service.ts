import { IAuthService } from "../interfaces/iauth-service.interface";
import { Users } from "src/users/entities/users.entity";
import { BaseParams } from "src/communs/entities/base-params";
import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { BaseService } from "src/communs/services/base.service";
import * as bcrypt from "bcrypt";
import { JwtService } from "@nestjs/jwt";
import { JwtPayload } from "../interfaces/jwt-payload.interface";
import { Request } from "express";
import { AppConfig } from "src/app.config";

@Injectable()
export class AuthService extends BaseService implements IAuthService{
    authParams: BaseParams = new BaseParams();
    constructor(private readonly jwtService: JwtService){
        super();
        this.authParams.currentRepository = 'Users';
        this.authParams.relations = ['permissions']; 
    }

    async authenticate(login: string, password: string): Promise<Users> {
        try{
            const authenticatedUser: Users = await this.findOne({login:login},this.authParams);
            await this.verifyPassword(password, authenticatedUser.password);
            return authenticatedUser;
        }catch(error){
            throw new HttpException('Wrong credentials provided', HttpStatus.UNAUTHORIZED);
        }
    }


    async generateAccessToken(user:Users): Promise<string> {
        const payload:JwtPayload = { username: user.login, password: user.password, sub: user.pk }
        return this.jwtService.sign(payload);
    }

    chekPermission(user: Users, req: Request): boolean{
        let permissionExist = false;
        const checkedEndPoint = req.originalUrl.replace(AppConfig.globalPath+'/','');
        user.permissions.forEach(permission => {
            if( permission.endPoint.toLowerCase() === checkedEndPoint.toLowerCase() 
                && permission.method.toLowerCase() === req.method.toLowerCase() ){
                    permissionExist = true;
                    return;
                }
        });
        return permissionExist;
    }

    async checkUser(login:string,password:string, req: Request):Promise<Users>{
        try{
            const authenticatedUser: Users = await this.findOne(
                {   
                    login:login
                },
                this.authParams, true);
            if(authenticatedUser.password === password){
                const checkUserPermission: boolean = await this.chekPermission(authenticatedUser,req);
                /* if(!checkUserPermission)    throw new HttpException('Unauthorized action',HttpStatus.UNAUTHORIZED); */
                return authenticatedUser;
            }
        }catch(Exception){
            throw new HttpException(Exception.message, HttpStatus.UNAUTHORIZED);
        }
    }

    private async verifyPassword(plainTextPassword: string, hashedPassword: string) {
        const isPasswordMatching = await bcrypt.compare(
            plainTextPassword,
            hashedPassword
        );
        if (!isPasswordMatching) {
            throw new HttpException('Wrong credentials provided', HttpStatus.UNAUTHORIZED);
        }
    }
}