import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AppConfig } from 'src/app.config';
import { AuthController } from './controllers/auth.controller';
import { BaseService } from 'src/communs/services/base.service';
import { AuthService } from './services/auth.service';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './services/local.strategy';
import { JwtStrategy } from './services/jwt.strategy';

@Module({
    imports: [
        PassportModule.register({      
            defaultStrategy: 'jwt',      
            property: 'user',      
            session: false,    
        }),
        JwtModule.register({
            secret: AppConfig.jwtSecret,
            signOptions: {
              expiresIn: AppConfig.jwtLifeTime,
            },
        }),
    ],
    controllers: [ AuthController ],
    providers:[
        BaseService,
        AuthService,
        LocalStrategy,
        JwtStrategy
    ]
})
export class AuthModule {}
