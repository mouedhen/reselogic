import { Users } from "src/users/entities/users.entity";
import { Request } from "express";

export interface IAuthService{
    authenticate(login: string, password: string): Promise<Users>;
    generateAccessToken(user:Users):Promise<any>;
    checkUser(login: string, password: string,req: Request): Promise<Users>;
}