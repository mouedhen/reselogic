import { Entity, Column, OneToMany } from "typeorm";
import { BaseEntity } from "src/communs/entities/base-entity";
import { VehiculeTypes } from "./vehicules-type.enum";
import { Employees } from "src/employees/entities/employee.entity";

@Entity()
export class Vehicules extends BaseEntity{
    @Column({ unique: true })
    registrationNumber: string;
    @Column()
    type: VehiculeTypes;
    @Column({ nullable: true })
    purchasePrice: Number;
    @Column()
    startDate: Date;
    @Column({ nullable: true })
    endDate: Date;
    @Column({ nullable: true })
    state: string;
    
    @OneToMany(type => Employees, employee => employee.vehicule)
    employee: Employees[];
}