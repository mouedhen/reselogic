export enum VehiculeTypes{
    CAR="CAR",
    VAN="VAN",
    TRUCK="TRUCK",
    MOTOCYCLE="MOTOCYCLE",
    BIKE="BIKE"
}