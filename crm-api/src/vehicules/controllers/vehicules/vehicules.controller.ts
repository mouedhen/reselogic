import { Controller, UseGuards, Post, Body, Req, Get, Put } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/services/jwt-auth.guard';
import { Vehicules } from 'src/vehicules/entities/vehicules.entity';
import { VehiculesService } from 'src/vehicules/services/vehicules/vehicules.service';
import { Users } from 'src/users/entities/users.entity';
import { Request } from 'express';
import { BaseParams } from 'src/communs/entities/base-params';

@UseGuards(JwtAuthGuard)
@Controller('vehicules')
export class VehiculesController {
    
    vehiculesParams: BaseParams = new BaseParams();
    constructor(private readonly vehiculeService: VehiculesService){
        this.vehiculesParams.currentRepository = 'Vehicules'; 
    }

    @Post()
    async addVehicule(@Req() req: Request, @Body() vehicule: Vehicules): Promise<Vehicules>{
        const currentUser:Users = <Users>(req.user);
        return await this.vehiculeService.add(vehicule, this.vehiculesParams, currentUser);
    }

    @Get()
    async listVehicules(): Promise<Vehicules[]>{
        return await this.vehiculeService.findAll(this.vehiculesParams);
    }

    @Put()
    async updateVehicule(@Req() req: Request, @Body() vehicule: Vehicules): Promise<Vehicules>{
        const currentUser:Users = <Users>(req.user);
        return this.vehiculeService.update(vehicule, vehicule.pk, this.vehiculesParams, currentUser);
    }

}
