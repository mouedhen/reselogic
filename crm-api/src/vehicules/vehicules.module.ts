import { Module } from '@nestjs/common';
import { VehiculesService } from './services/vehicules/vehicules.service';
import { VehiculesController } from './controllers/vehicules/vehicules.controller';
import { AuthModule } from 'src/auth/auth.module';

@Module({
    imports: [AuthModule],
    controllers: [ VehiculesController ],
    providers: [ VehiculesService ]
})
export class VehiculesModule {}
