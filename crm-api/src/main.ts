import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AppConfig } from './app.config';
import * as helmet from 'helmet';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.use(helmet());
  /* app.use(cookieParser())
  app.use(csruf({ cookie: true })); */
  app.setGlobalPrefix(AppConfig.globalPath);
  await app.listen(AppConfig.portToListen);
}
bootstrap();
