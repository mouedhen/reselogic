import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ProductsModule } from './products/products.module';
import { EmployeesModule } from './employees/employees.module';
import { ClientModule } from './client/client.module';
import { OrdersModule } from './orders/orders.module';
import { VehiculesModule } from './vehicules/vehicules.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrmConfig } from './communs/database/orm-config.config';
import { ConfigModule } from '@nestjs/config';
import * as Joi from '@hapi/joi';

@Module({
  imports: [
    AuthModule, 
    UsersModule, 
    ProductsModule, 
    EmployeesModule, 
    ClientModule, 
    OrdersModule, 
    VehiculesModule,
    TypeOrmModule.forRoot(
      {
          type: 'mysql',
          host: OrmConfig.host,
          port: OrmConfig.port,
          username: OrmConfig.username,
          password: OrmConfig.password,
          database: OrmConfig.database,
          entities: OrmConfig.entities,
          synchronize: OrmConfig.synchronize,
      }
    )
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
